<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

$correctPassword = 'poussiere';

$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, true);

if (isset($input['checkPassword'])) {
    if (!throttleCheck()) {
        return;
    }
    if (isset($input['password']) && $input['password'] === $correctPassword) {
        echo json_encode(["status" => "success"]);
    } else {
        echo json_encode(["status" => "error", "message" => "Invalid password"]);
    }
    return;
} else if (isset($input['password']) && $input['password'] === $correctPassword) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($input['get'])) {
            if (isset($input['rating'])) {
                getData($input, 'ratings.json');
            } else if (isset($input['recommandation'])) {
                getData($input, 'recommandations.json');
            }
            return;
        }
        if (isset($input['delete'])) {
            if (isset($input['rating'])) {
                delData($input, 'ratings.json');
            } else if (isset($input['recommandation'])) {
                delData($input, 'recommandations.json');
            }
            return;
        }
        if (isset($input['movie'])) {
            if (isset($input['rating'])) {
                postRating($input, 'ratings.json');
            } else if (isset($input['recommandation'])) {
                postRating($input, 'recommandations.json');
            }
            return;
        }
    }
}

function throttleCheck() {
    $filePath = 'protection.json';
    $ip = $_SERVER['REMOTE_ADDR'];
    $now = time();

    if (file_exists($filePath)) {
        $data = json_decode(file_get_contents($filePath), true);
    } else {
        $data = [];
    }

    if (isset($data[$ip]) && ($now - $data[$ip]) < 1) {
        echo json_encode(["status" => "error", "message" => "Try again in a few seconds"]);
        return false;
    } else {
        $data[$ip] = $now;
        file_put_contents($filePath, json_encode($data));
        return true;
    }
}

function delData($input, $filename = null) {
    if ($filename !== null && isset($input['movie']) && isset($input['movie']['imdbID'])) {
        $currentData = json_decode(file_get_contents($filename), true);
        $titleToDelete = $input['movie']['imdbID'];
        if ($currentData === null) {
            $newData = [];
        } else {
            $newData = array_filter($currentData, function($movie) use ($titleToDelete) {
                return $movie['movie']['imdbID'] !== $titleToDelete;
            });
        }

        file_put_contents($filename, json_encode(array_values($newData), JSON_PRETTY_PRINT));
        echo json_encode(["status" => "success"]);
    } else {
        echo json_encode(["status" => "error", "message" => "Invalid data", "data" => $input]);
    }
}

function postRating($input, $filename = null) {
    if ($filename !== null) {
        $currentData = json_decode(file_get_contents($filename), true);
        if ($currentData === null) {
            $currentData = [];
        }
        
        $input['password'] = null;
        unset($input['password']);
        $currentData[] = $input;

        file_put_contents($filename, json_encode($currentData, JSON_PRETTY_PRINT));
        echo json_encode(["status" => "success"]);
    } else {
        echo json_encode(["status" => "error", "message" => "Invalid data", "data" => $input]);
    }
}

function getData($input, $filename = null) {
    if ($filename !== null && file_exists($filename)) {
        $data = json_decode(file_get_contents($filename), true);
        echo json_encode($data, JSON_PRETTY_PRINT);
    } else {
        echo json_encode(["status" => "error", "message" => "File not found"]);
    }
}


?>