# CineArchive

## Features :


## A faire

-> mettre le bandeau en activable / désactivable
-> transition changement d'onglets
-> changer la gueule des champs d'ajout de note
-> limiter la taille du texte si le cadre est trop petit ? (titres très longs)
-> retravailler la recherche pour pouvoir aussi balancer des réal (X de Ti West)
-> ajouter un comportement de scroll en cliquant / glissant
-> onglet sélectionné -> barre en dessous à faire disparaitre et remonter un peu l'onglet sélectionné (le descendre sur un hover)
-> ajouter durée des films sur le tooltip
-> sur un double espace, mettre le focus sur le champ de recherche

## Fait

-> cliquer sur une note ou une recommandation pour la supprimer (canvas pour cliquer en croix ?)
-> export devient settings, et changement de thème
-> couleurs des notes en fonction du thème
-> afficher le nombre de films displayed
-> date du vu le, en toute lettres (14 aout 2023)
-> bouton "all" pour revenir à la liste complète 
-> titres déroulables pour cacher le contenu
-> trier les genres par ordre alphabétique
-> changer les thèmes par ces couleurs unifiées
-> s'occuper d'unifier les couleurs
-> adapter la liste des genres à "movies seen"
-> retravailler le visuel de l'ajout, pouvoir chercher un film en appuyant sur entrée
-> pouvoir ajouter the machine, child's play, freaks, hellraiser, hypnotic, creep (retravailler la recherche)
-> recherche en appuyant sur entrée
-> mettre en place une recherche
-> focus sur le champ password en arrivant sur la page
-> si un film est déjà noté, on ne peut le recommander