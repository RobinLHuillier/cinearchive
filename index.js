//----------------------------
//                     GLOBALS
//----------------------------

const flags = [false, false];

let movieData = {};
let movieSeenCallbacks = [];
const listMoviesSeenUpToDate = 0;
let listMoviesSeen = [];

let movieRecommandationsCallbacks = [];
const listMoviesRecommandationsUpToDate = 1;
let listMoviesRecommandations = [];

let currentMoviesSelection = [];
let password = "";

const refMenu = {
    'addMovie': document.getElementById('addMovie'),
    'moviesSeen': document.getElementById('moviesSeen'),
    'moviesRecommandation': document.getElementById('moviesRecommandation'),
    'statistics': document.getElementById('statistics'),
    'settings': document.getElementById('settings'),
}
const mainBody = document.body;
const body = document.getElementById('body');
const menu = document.getElementById('menu');

const TYPE_CARD_RECOMMANDATION = 'recommandation';
const TYPE_CARD_RESEARCH = 'research';
const TYPE_CARD_SEEN = 'seen';

const canvasComp = document.getElementById('canvas-comportement');
const ctxComp = canvasComp.getContext('2d');
canvasComp.width = canvasComp.clientWidth;
canvasComp.height = canvasComp.clientHeight;

const comportement = {
    div: null,
    points: [],
};
const mousePos = {
    x: 0,
    y: 0,
};
document.addEventListener('mousemove', (e) => {
    mousePos.x = e.clientX;
    mousePos.y = e.clientY;
});

const colors = {};
computeColors();

//----------------------------
//                     LOADING
//----------------------------

function startLoadingEverything() {
    getMoviesSeen(() => executeAllCallbacks(movieSeenCallbacks, listMoviesSeenUpToDate));
    getMoviesRecommandations(() => executeAllCallbacks(movieRecommandationsCallbacks, listMoviesRecommandationsUpToDate));
}

function executeAllCallbacks(callbacks, flag) {
    flags[flag] = true;
    for (let i = 0; i < callbacks.length; i++) {
        callbacks[i]();
    }
    callbacks = [];
}

function addCallbackMovieSeen(callback) {
    if (flags[listMoviesSeenUpToDate]) {
        callback();
    } else {
        movieSeenCallbacks.push(callback);
    }
}

function addCallbackMovieRecommandations(callback) {
    if (flags[listMoviesRecommandationsUpToDate]) {
        callback();
    } else {
        movieRecommandationsCallbacks.push(callback);
    }
}

//----------------------------
//                   API CALLS
//----------------------------

function getMovieByIMDBId(callback) {
    if (!!movieData.imdbID) {
        fetch(`http://www.omdbapi.com/?apikey=${omdbkey}&i=${movieData.imdbID}&plot=full&r=json`)
            .then(response => response.json())
            .then(data => {
                movieData = data;
            })
            .then(callback)
            .catch(err => {
                console.log('Erreur:', err);
            });
    } else {
        alert('Pas de film sélectionné');
    } 
}

function searchMovie() {
    const movieTitle = document.getElementById('movieTitle').value;
    const apiKey = omdbkey;
    const movieInfo = document.getElementById('movieInfo');
    movieInfo.innerHTML = '';
    movieInfo.appendChild(createLocalResearchPaddingDiv());
    const movieSelect = document.getElementById('movieSelect');
    movieSelect.className = 'tooltip hidden';
    const movieSelection = document.getElementById('movieSelection');

    fetch(`http://www.omdbapi.com/?apikey=${apiKey}&s=${movieTitle}&plot=full&r=json`)
      .then(response => response.json())
      .then(data => {
            if (data.Response === "True") {
                console.log(data);
                movieSelection.innerHTML = `
                    <button onclick="getMovieByIMDBId(addRating)">Ajouter une note</button>
                    <button onclick="getMovieByIMDBId(postRecommandation)">Recommander</button>
                `;
                for (let movie of data.Search) {
                    const m = {'movie': movie};
                    const movieCard = createMovieDiv(m, TYPE_CARD_RESEARCH);
                    movieCard.className = 'movieCard research';
                    movieCard.onclick = () => {
                        movieInfo.querySelectorAll('.movieCard').forEach(card => card.className = 'movieCard research');
                        movieData = movie;
                        console.log(movieData);
                        movieCard.className = 'movieCard research selected';
                        movieSelect.classList.remove('hidden');
                    };
                    movieInfo.appendChild(movieCard);
                    if (data.Search.length === 1) {
                        movieCard.click();
                    }
                }
            } else {
                alert('Film introuvable');
            }
      })
      .catch(err => {
            console.log('Erreur:', err);
      });
}

function postRating(movieData, rating, date) {
    for (let movie of listMoviesSeen) {
        if (movie.movie.Title === movieData.Title) {
            alert('Film déjà noté');
            return;
        }
    }
    for (let movie of listMoviesRecommandations) {
        if (movie.movie.Title === movieData.Title) {
            delRecommandation(movie.movie);
        }
    }
    if (rating >= 0 && rating <= 10) {
        const postData = {
            'password': password,
            'movie': movieData,
            'rating': rating,
            'date': date,
        };
        fetch(addressApi, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(postData)
        })
            .then(response => response.json())
            .then(data => {
                listMoviesSeen.push(postData);
                alertMovieAdded();
                resetResearch();
            })
            .catch(err => {
                console.error(err);
                alertMovieProblem();
            });
    } else {
        alert('Mets une note entre 0 et 10.');
    }
}

function postRecommandation() {
    for (let movie of listMoviesRecommandations) {
        if (movie.movie.Title === movieData.Title) {
            alert('Film déjà recommandé');
            return;
        }
    }
    for (let movie of listMoviesSeen) {
        if (movie.movie.Title === movieData.Title) {
            alert('Film déjà noté');
            return;
        }
    }
    const postData = {
        'password': password,
        'movie': movieData,
        'recommandation': true,
    };
    fetch(addressApi, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(postData)
    })
        .then(response => response.json())
        .then(data => {
            listMoviesRecommandations.push(postData);
            alertMovieAdded();
            resetResearch();
        })
        .catch(err => {
            console.error(err);
            alertMovieProblem();
        });
}

function delRating(data = null, callback = null) {
    delData({
        'password': password,
        'movie': data ?? movieData,
        'rating': true,
        'delete': true,
    }, () => {
        listMoviesSeen = listMoviesSeen.filter(movie => movie.movie.Title !== (data.Title ?? movieData.Title));
    }, callback);
}

function delRecommandation(data = null, callback=()=>{}) {
    delData({
        'password': password,
        'movie': data ?? movieData,
        'recommandation': true,
        'delete': true,
    }, () => {
        listMoviesRecommandations = listMoviesRecommandations.filter(movie => movie.movie.Title !== (data.Title ?? movieData.Title));
    }, callback);
}

function delData(data, callback1, callback2=()=>{}) {
    fetch(addressApi, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(data)
    })
        .then(response => response.json())
        .then(callback1)
        .then(callback2)
        .then(() => success('Film supprimé !'))
        .catch(err => {
            console.error(err);
            alert('Erreur lors de la suppression du film');
        });
}

function getMoviesSeen(callback) {
    const postData = {
        'password': password,
        'get': 1,
        'rating': 'yes',
    };
    fetch(addressApi, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(postData)
    })
        .then(response => response.json())
        .then(data => listMoviesSeen = data)
        .then(callback);
}

function getMoviesRecommandations(callback) {
    const postData = {
        'password': password,
        'get': 1,
        'recommandation': 'yes',
    };
    fetch(addressApi, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(postData)    
    })
        .then(response => response.json())
        .then(data => listMoviesRecommandations = data)
        .then(callback);
}

function checkPassword() {
    const passwordInput = document.getElementById('checkPasswordInput')?.value;
    if (!!passwordInput && passwordInput.length > 0) {
        const postData = {
            'password': passwordInput,
            'checkPassword': true,
        };
        fetch(addressApi, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(postData)
        })
            .then(response => response.json())
            .then(data => {
                if (data.status === 'success') {
                    success('Mot de passe correct');
                    password = passwordInput;
                    document.getElementById('checkPassword').remove();
                    document.getElementById('body').classList.remove('hidden');
                    startLoadingEverything();
                    addCallbackMovieSeen(() => menuClick('moviesSeen'));
                } else if (data.message === "Invalid password") {
                    alert('Mot de passe incorrect');
                } else if (data.message === "Try again in a few seconds") {
                    alert("Trop de tentatives, réessaye dans quelques secondes");
                } else {
                    console.error(data);
                    alert('Erreur lors de la vérification du mot de passe');
                }
            })
            .catch(err => {
                console.error(err);
                alert('Mot de passe incorrect');
            });

    } else {
        alert('Mot de passe vide');
    }
}

//----------------------------
//                DISTRIBUTION
//----------------------------

function menuClick(menuItem) {
    for (const key in refMenu) {
        if (key === menuItem) {
            refMenu[key].className = 'menu-selected';
        } else {
            refMenu[key].className = '';
        }
    }
    resetComportement();
    body.innerHTML = '';
    switch(menuItem) { 
        case 'addMovie':
            body.appendChild(createAddMovieDiv());
            break;
        case 'moviesSeen':
            body.appendChild(createMoviesSeenDiv());
            break;
        case 'moviesRecommandation':
            body.appendChild(createMoviesRecommandationDiv());
            break;
        case 'settings':
            body.appendChild(createSettingsDiv());
            break;
        case 'statistics':
            body.appendChild(createStatisticsDiv());
            break;
        default:
            break;
    }
}

//----------------------------
//                 CREATE HTML
//----------------------------

function addRating() {
    const ratingDiv = document.getElementById('movieRating');
    const ratingInput = document.createElement('input');
    ratingInput.type = 'number';
    ratingInput.min = 0;
    ratingInput.max = 10;
    ratingInput.step = 0.5;
    const dateInput = document.createElement('input');
    dateInput.type = 'date';
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    const yyyy = today.getFullYear();
    dateInput.value = `${yyyy}-${mm}-${dd}`;
    const confirmButton = document.createElement('button');
    confirmButton.innerHTML = 'Confirmer';
    confirmButton.onclick = () => postRating(movieData, parseFloat(ratingInput.value), dateInput.value);
    ratingDiv.innerHTML = ''; 
    ratingDiv.appendChild(ratingInput);
    ratingDiv.appendChild(dateInput);
    ratingDiv.appendChild(confirmButton);
}

function resetResearch() {
    menuClick('addMovie');
}

function createStatisticsDiv() {
    const statisticsDiv = document.createElement('div');
    const container = document.createElement('div');
    container.className = 'body-container-list-movies';
    statisticsDiv.appendChild(createBannerStatistics());
    statisticsDiv.appendChild(container);
    return statisticsDiv;
}

function createBannerStatistics() {
    const banner = document.createElement('div');
    banner.className = 'banner';
    const yearRecap = createBannerTitleDiv(`Récapitulatif annuel`, () => {
        seeYearRecap();
    });
    banner.appendChild(yearRecap);
    return banner;
}

function seeYearRecap() {
    const container = getContainerReseted();
    const canvas = document.createElement('canvas');
    canvas.className = 'canvasStats';
    const ctx = canvas.getContext('2d');
    container.appendChild(canvas);
    drawYearRecap(ctx, canvas);
}

function createSettingsDiv() {
    const settingsDiv = document.createElement('div');
    settingsDiv.className = 'settings';

    const buttonRatings = document.createElement('button');
    buttonRatings.innerHTML = 'Exporter les notes';
    buttonRatings.onclick = exportRatings;
    const buttonRecommandations = document.createElement('button');
    buttonRecommandations.innerHTML = 'Exporter les recommandations';
    buttonRecommandations.onclick = exportRecommandations;
    settingsDiv.appendChild(buttonRatings);
    settingsDiv.appendChild(buttonRecommandations);

    const selectTheme = document.createElement('select');
    selectTheme.onchange = () => changeTheme(selectTheme.value);
    const themes = ['eighties', 'roaring', 'pastel', 'pollen', 'dead-weight', 'gray-fruit', 'ammo-8', 'amphibian-8'];
    for (const theme of themes) {
        const option = document.createElement('option');
        option.value = theme;
        option.textContent = theme.charAt(0).toUpperCase() + theme.slice(1);
        selectTheme.appendChild(option);
    }
    const option = document.createElement('option');
    option.value = '';
    option.textContent = 'Changer le thème';
    option.disabled = true;
    option.selected = true;
    selectTheme.insertBefore(option, selectTheme.firstChild);

    settingsDiv.appendChild(selectTheme);

    return settingsDiv;
}

function createAddMovieDiv() {
    const addMovieDiv = document.createElement('div');
    addMovieDiv.innerHTML = `
        <div class="localResearch">
            <input type="text" id="movieTitle" autofocus placeholder="Titre du film" onkeydown="if (event.keyCode == 13) searchMovie()" class="localResearchInput">
        </div>
        <div id="movieInfo" class="body-container-list-movies"></div>
        <div id="movieSelect" class="tooltip hidden">
            <div id="movieSelection"></div>
            <div id="movieRating"></div>
        </div>
    `;
    return addMovieDiv;
}

function createMoviesSeenDiv() {
    const moviesSeenDiv = document.createElement('div');
    const forRecommandation = false;
    moviesSeenDiv.appendChild(createBannerSort(listMoviesSeen, forRecommandation));
    const container = document.createElement('div');
    container.className = 'body-container-list-movies';
    moviesSeenDiv.appendChild(createLocalResearchDiv(listMoviesSeen));
    moviesSeenDiv.appendChild(container);
    container.appendChild(createLocalResearchPaddingDiv());
    addCallbackMovieSeen(() => {
        listMoviesSeen.sort((a, b) => {
            return new Date(b.date) - new Date(a.date);
        }).forEach(movie => {
            container.appendChild(createMovieDiv(movie, TYPE_CARD_SEEN));
        });
    });
    return moviesSeenDiv;
}

function createMoviesRecommandationDiv() {
    const moviesRecommandationDiv = document.createElement('div');
    moviesRecommandationDiv.appendChild(createBannerSort(listMoviesRecommandations));
    const container = document.createElement('div');
    container.className = 'body-container-list-movies';
    moviesRecommandationDiv.appendChild(createLocalResearchDiv(listMoviesRecommandations, true));
    moviesRecommandationDiv.appendChild(container);
    container.appendChild(createLocalResearchPaddingDiv());
    addCallbackMovieRecommandations(() => {
        listMoviesRecommandations.forEach(movie => {
            container.appendChild(createMovieDiv(movie, TYPE_CARD_RECOMMANDATION));
        });
    });
    return moviesRecommandationDiv;
}

//----------------------------
//             COMMON CREATION
//----------------------------

function createBannerTitleDiv(title, onclick = null) {
    const banner = document.createElement('div');
    banner.className = 'banner-title';
    const bannerTitle = document.createElement('h3');
    bannerTitle.textContent = title;
    banner.appendChild(bannerTitle);
    if (null === onclick) {
        bannerTitle.onclick = () => {
            if (banner.className.includes('selected')) {
                banner.className = 'banner-title';
            } else {
                banner.className = 'banner-title selected';
            }
        }
    } else {
        bannerTitle.onclick = onclick;
    }
    return banner;
}

function createGenreDiv(list, forRecommandation) {
    const genres = getAllGenre(list);
    const genreDiv = createBannerTitleDiv('Genres');
    const genreContent = document.createElement('div');
    genreContent.className = 'banner-content';
    genreDiv.appendChild(genreContent);

    const alphabeticalGenres = Object.keys(genres).sort((a, b) => a.localeCompare(b));
    for (const genre of alphabeticalGenres) {
        const genreClickable = document.createElement('div');
        genreClickable.className = 'banner-item';
        genreClickable.onclick = () => {
            currentMoviesSelection = list.filter(movie => movie.movie.Genre.includes(genre));
            updateHTMLMoviesByListSelected(forRecommandation);
        }
        genreClickable.textContent = `${genre} (${genres[genre]})`;
        genreContent.appendChild(genreClickable);
    }
    return genreDiv;
}

function createBannerByYearDiv(list, forRecommandation) {
    const years = sortMoviesByYear(list);
    const yearsDiv = createBannerTitleDiv('Années');
    const yearsContent = document.createElement('div');
    yearsContent.className = 'banner-content';
    yearsDiv.appendChild(yearsContent);

    const sortedYears = Object.keys(years).sort((a, b) => a < b);
    for (const year of sortedYears) {
        const yearClickable = document.createElement('div');
        yearClickable.className = 'banner-item';
        yearClickable.onclick = () => {
            currentMoviesSelection = years[year];
            updateHTMLMoviesByListSelected(forRecommandation);
        }
        yearClickable.textContent = `${year} (${years[year].length})`;
        yearsContent.appendChild(yearClickable);
    }
    return yearsDiv;
}

function createBannerSort(list, forRecommandation=true) {
    const banner = document.createElement('div');
    banner.className = 'banner';
    const getAllDiv = createBannerTitleDiv(`Tout voir (${list.length})`, () => {
        currentMoviesSelection = list;
        updateHTMLMoviesByListSelected(forRecommandation);
    });
    banner.appendChild(getAllDiv);
    banner.appendChild(createGenreDiv(list, forRecommandation));    
    banner.appendChild(createBannerByYearDiv(list, forRecommandation));
    return banner;
}

function updateHTMLMoviesByListSelected(recommandation=false) {
    const container = getContainerReseted();
    const type = recommandation ? TYPE_CARD_RECOMMANDATION : TYPE_CARD_SEEN;
    currentMoviesSelection.forEach(movie => {
        container.appendChild(createMovieDiv(movie, type));
    });
}

function getContainerReseted() {
    resetComportement();
    const container = document.querySelector('.body-container-list-movies');
    container.innerHTML = '';
    container.appendChild(createLocalResearchPaddingDiv());
    return container;
}

function createMovieDiv(movie, type=TYPE_CARD_SEEN) {
    const title = movie.movie.Title;
    const imgSrc = movie.movie.Poster;
    const seen = new Date(movie.date);
    const seenDate = `${seen.getDate()} ${seen.toLocaleString('default', { month: 'long' })} ${seen.getFullYear()}`;
    const genre = movie.movie.Genre;
    const year = movie.movie.Year;
    const rating = movie.rating;

    const movieDiv = document.createElement('div');
    movieDiv.className = 'movieCard';
  
    const titleElement = document.createElement('h3');
    titleElement.textContent = title;
    movieDiv.appendChild(titleElement);
  
    const imgElement = document.createElement('img');
    imgElement.src = imgSrc;
    movieDiv.appendChild(imgElement);
    
    if (type === TYPE_CARD_RECOMMANDATION) {
        const dateElement = document.createElement('p');
        dateElement.textContent = `${year}`;
        movieDiv.appendChild(dateElement);

        const genreElement = document.createElement('p');
        genreElement.textContent = `${genre}`;
        movieDiv.appendChild(genreElement);
    } else if (type === TYPE_CARD_SEEN) {
        const dateElement = document.createElement('p');
        dateElement.textContent = `Vu le ${seenDate}`;
        movieDiv.appendChild(dateElement);
    
        const ratingElement = document.createElement('div');
        for (let i = 1; i <= 10; i++) {
            const star = document.createElement('span');
            if (i <= Math.floor(rating)) {
                star.textContent = '★';  
            } else if (i === Math.ceil(rating) && !Number.isInteger(rating)) {
                star.textContent = '½';
            } else {
                star.textContent = '☆';
            }
            ratingElement.appendChild(star);
        }
        movieDiv.appendChild(ratingElement);
        
        const color = getRGBColorFromRating(rating);
        movieDiv.style.backgroundColor = color;
        movieDiv.style.borderColor = color;
    } else if (type === TYPE_CARD_RESEARCH) {
        const dateElement = document.createElement('p');
        dateElement.textContent = `${year}`;
        movieDiv.appendChild(dateElement);
    }

    if (type === TYPE_CARD_RECOMMANDATION || type === TYPE_CARD_SEEN) {
        movieDiv.addEventListener('mouseenter', () => {
            movieDiv.querySelector('.tooltip').classList.remove('hidden');
        });
        movieDiv.addEventListener('mouseleave', () => {
            movieDiv.querySelector('.tooltip').classList.add('hidden');
        });
        movieDiv.appendChild(createTooltipMovie(movie));

        movieDiv.onmousedown = () => startClickOnCard(movieDiv, movie, type);
        movieDiv.onmouseup = () => endClickOnCard(movieDiv, movie, type);
    }

    return movieDiv;
}

function createTooltipMovie(movie) {
    const tooltipDiv = document.createElement('div');
    tooltipDiv.className = 'tooltip hidden';
    // genre
    const genre = document.createElement('p');
    genre.textContent = `Genre : ${movie.movie.Genre}`;
    tooltipDiv.appendChild(genre);
    // plot
    const plot = document.createElement('p');
    plot.textContent = `Synopsis : ${movie.movie.Plot}`;
    tooltipDiv.appendChild(plot);
    // année de sortie
    const year = document.createElement('p');
    year.textContent = `Sorti en : ${movie.movie.Year}`;
    tooltipDiv.appendChild(year);
    // director
    const director = document.createElement('p');
    director.textContent = `Réalisateur : ${movie.movie.Director}`;
    tooltipDiv.appendChild(director);
    // actors
    const actors = document.createElement('p');
    actors.textContent = `Acteurs : ${movie.movie.Actors}`;
    tooltipDiv.appendChild(actors);
    // writers
    const writers = document.createElement('p');
    writers.textContent = `Scénaristes : ${movie.movie.Writer}`;
    tooltipDiv.appendChild(writers);

    return tooltipDiv;
}

function createLocalResearchPaddingDiv() {
    const div = document.createElement('div');
    div.className = 'localResearchPadding';
    return div;
}

function createLocalResearchDiv(list, forRecommandation=false) {
    const localResearchDiv = document.createElement('div');
    localResearchDiv.className = 'localResearch';
    const localResearchInput = document.createElement('input');
    localResearchInput.type = 'text';
    localResearchInput.placeholder = 'Rechercher un film, un acteur, un réalisateur ...';
    localResearchInput.className = 'localResearchInput'
    localResearchDiv.appendChild(localResearchInput);
    localResearchInput.onkeyup = (e) => {
        const res = locallySearchMovie(list, e.target.value);
        if (!compareTwoList(res, currentMoviesSelection)) {
            currentMoviesSelection = res;
            updateHTMLMoviesByListSelected(forRecommandation);
        }
    };
    return localResearchDiv;
}

//----------------------------
//                        SORT
//----------------------------

function splitAndLower(str) {
    return str.toLowerCase().split(' ');
}

function isBagOfWordsIncluded(searchWords, bag) {
    const bagWords = splitAndLower(bag);
    for (let searchWord of searchWords) {
        let found = false;
        for (let word of bagWords) {
            if (found) {
                continue;
            }
            if (word.includes(searchWord)) {
                found = true;
            }
        }
        if (!found) {
            return false;
        }
    }
    return true;
}

function locallySearchMovie(list, searchWords) {
    if (!searchWords || searchWords.length === 0) {
        return list;
    }
    const searchs = splitAndLower(searchWords);
    return list.filter(movie => {
        for (let bags of [movie.movie.Title, movie.movie.Director, movie.movie.Actors, movie.movie.Writer, movie.movie.Genre]) {
            if (isBagOfWordsIncluded(searchs, bags)) {
                return true;
            }
        }
        return false;
    });
}

function compareTwoList(list1, list2) {
    if (list1.length !== list2.length) {
        return false;
    }
    for (let i = 0; i < list1.length; i++) {
        if (!list2.includes(list1[i]) || !list1.includes(list2[i])) {
            return false;
        }
    }
    return true;
}

function sortMoviesByYear(list) {
    const moviesByYear = {};
    for (let movie of list) {
        const year = movie.movie.Year;
        if (!Object.keys(moviesByYear).includes(year)) {
            moviesByYear[year] = [];
        }
        moviesByYear[year].push(movie);
    }
    return moviesByYear;
}

function getAllGenre(list) {
    const genreList = {};
    for (let movie of list) {
        for (let genre of movie.movie.Genre.split(', ')) {
            if (!Object.keys(genreList).includes(genre)) {
                genreList[genre] = 0;
            }
            genreList[genre]++;
        }
    }
    return genreList;
}

//----------------------------
//                      CANVAS
//----------------------------

function drawYearRecap(ctx, canvas) {
    // calculate real size of the canvas
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    canvas.width = width;
    canvas.height = height;
    
    const moviesSeenByYear = sortMoviesByYear(listMoviesSeen);
    const years = Object.keys(moviesSeenByYear);
    const minYear = Math.min(...years);
    const maxYear = Math.max(...years);
    const maxMovies = Math.max(...Object.values(moviesSeenByYear).map(list => list.length));
    const nbYears = maxYear - minYear + 1;
    
    const widthYear = width / nbYears;
    strokeLine(ctx, 0, height-30, width, height-30, colors.night, 1);
    const heightMovie = (height-50) / maxMovies;
    for (let i = 0; i < nbYears; i++) {
        const year = minYear + i;
        const x = (i + 0.5) * widthYear;
        strokeLine(ctx, x, height-40, x, height-20, colors.night);
        centerText(ctx, year, x, height-10, 10, colors.night);
        const movies = moviesSeenByYear[year]?.length ?? 0;
        if (movies > 0) {
            const y = height - 30 - movies * heightMovie;
            ctx.fillStyle = colors.forest;
            ctx.fillRect(x - widthYear/2 + 3, y, widthYear-6, movies * heightMovie);
            centerText(ctx, movies, x, y + movies * heightMovie/2, 15, colors.yellow, 'bold');
        }
    }    
}

function centerText(ctx, text, x, y, fontSize, color, variant='') {
    ctx.font = `${variant} ${fontSize}px Arial`;
    ctx.fillStyle = color;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillText(text, x, y);
}

function strokeLine(ctx, x1, y1, x2, y2, color, width, round=false) {
    ctx.strokeStyle = color;
    if (round) {
        ctx.lineCap = "round";
    }
    ctx.lineWidth = width;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

//----------------------------
//         CANVAS COMPORTEMENT
//----------------------------

function eraseCanvasComportement() {
    ctxComp.clearRect(0, 0, canvasComp.width, canvasComp.height);
}

function drawLineCanvasComportement() {
    if (comportement.points.length < 2) {
        return;
    }
    strokeLine(
        ctxComp, 
        comportement.points.at(-2).x, 
        comportement.points.at(-2).y, 
        comportement.points.at(-1).x, 
        comportement.points.at(-1).y, 
        colors.purple, 5, true);
    strokeLine(
        ctxComp, 
        comportement.points.at(-2).x, 
        comportement.points.at(-2).y, 
        comportement.points.at(-1).x, 
        comportement.points.at(-1).y, 
        colors.ocre, 3, true);
    setTimeout(() => {
        if (comportement.points.length === 2) {
            eraseCanvasComportement();
        }
    }, 3000);
}

function calculateSlope(p1, p2) {
    return (p1.y - p2.y) / (p1.x - p2.x);
}

function checkForDeleteComportement() {
    const slope1 = calculateSlope(comportement.points[0], comportement.points[1]);
    const slope2 = calculateSlope(comportement.points[2], comportement.points[3]);
    return (slope1 * slope2 < 0);
}

function resetComportement() {
    eraseCanvasComportement();
    comportement.div = null;
    comportement.points = [];
}

function startClickOnCard(movieDiv, movie, type) {
    if (movieDiv !== comportement.div) {
        resetComportement();
        comportement.div = movieDiv;
    }
    comportement.points.push({x: mousePos.x, y: mousePos.y});
}

function endClickOnCard(movieDiv, movie, type) {
    if (movieDiv !== comportement.div) {
        resetComportement();
        return;
    }
    comportement.points.push({x: mousePos.x, y: mousePos.y});
    drawLineCanvasComportement();
    if (comportement.points.length === 4) {
        if (checkForDeleteComportement()) {
            const callback = () => {
                resetComportement();
                menuClick(type === TYPE_CARD_RECOMMANDATION ? 'moviesRecommandation' : 'moviesSeen');
            }
            setTimeout(() => {
                if (type === TYPE_CARD_RECOMMANDATION) {
                    delRecommandation(movie.movie, callback);
                } else if (type === TYPE_CARD_SEEN) {
                    delRating(movie.movie, callback);
                }
            }, 1000);
        } else {
            setTimeout(() => {
                resetComportement();
            }, 1000);
        }
    }
}

//----------------------------
//                      POPUPS
//----------------------------

function success(text) {
    addPopUp(text, 'success');
}

function alert(text) {
    addPopUp(text, 'danger');
}

function alertMovieAdded() {
    addPopUp('Film ajouté !', 'success');
}

function alertMovieProblem() {
    addPopUp('Erreur lors de l\'ajout du film', 'danger');
}

function addPopUp(text, type='success') {
    const popUp = document.createElement('div');
    if (type === 'success') {
        popUp.className = 'popUp success';
    } else {
        popUp.className = 'popUp danger';
    }
    popUp.textContent = text;
    mainBody.appendChild(popUp);
    setTimeout(() => {
        popUp.classList.add('show');
    }, 100);

    setTimeout(() => {
        popUp.classList.remove('show');
        setTimeout(() => {
            popUp.remove();
        }, 300);
    }, 3000);
}

//----------------------------
//                      EXPORT
//----------------------------

function exportRecommandations() {
    exportData(listMoviesRecommandations, 'recommandations.json');
}

function exportRatings() {
    exportData(listMoviesSeen, 'ratings.json');
}

function exportData(data, fileName) {
    const blob = new Blob([JSON.stringify(data)], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    a.click();
    URL.revokeObjectURL(url);
}

//----------------------------
//                       THEME
//----------------------------

function changeTheme(theme) {
    let colorsList = [];
    let names = ['--night', '--purple', '--ocre', '--gold', '--yellow', '--weed', '--salad', '--forest'];
    switch(theme) {
        case 'roaring':
            colorsList = ['#01000e', '#330043', '#a40038', '#d43e1f', '#f5c1c5', '#df892e', '#4e7026', '#09177e'];
            break;
        case 'pastel':
            colorsList = ['#004343', '#74a892', '#c7522a', '#e5c185', '#f0daa5', '#fbf2c4', '#b8cdab', '#008585'];
            break;
        case 'pollen':
            colorsList = ['#73464c', '#ab5675', '#ee6a7c', '#ffa7a5', '#ffe07e', '#ffe7d6', '#72dcbb', '#34acba'];
            break;
        case 'dead-weight':
            colorsList = ['#24171b', '#5d453e', '#902e29', '#a4653e', '#debf89', '#907c68', '#8a8e48', '#495435'];
            break;
        case 'gray-fruit':
            colorsList = ['#000000', '#2e1f49', '#623ea2', '#9b20b7', '#ffffff', '#e53aff', '#2eff6c', '#1d775d'];
            break;
        case 'ammo-8':
            colorsList = ['#040c06', '#112318', '#305d42', '#89a257', '#eeffcc', '#bedc7f', '#4d8061', '#1e3a29'];
            break;
        case 'amphibian-8':
            colorsList = ['#171015', '#332628', '#633b2c', '#a67935', '#a8a743', '#a7b593', '#476b42', '#2a453d'];
            break;
        case 'eighties':
        default:
            colorsList = ['#1f244b', '#654053', '#a8605d', '#d1a67e', '#f6e79c', '#b6cf8e', '#60ae7b', '#3c6b64'];
            break;
    }
    for (let i=0; i<colorsList.length; i++) {
        document.documentElement.style.setProperty(names[i], colorsList[i]);
    }
    computeColors();
}

function getRGBColorFromRating(rating) {
    return mixColors(colors.salad, colors.ocre, Math.max(0, (-2+rating)/8));
}

function mixColors(color1, color2, ratio=0.5) {
    const c1 = hexToRgb(color1);
    const c2 = hexToRgb(color2);

    const r = Math.round(c1.r * ratio + c2.r * (1 - ratio));
    const g = Math.round(c1.g * ratio + c2.g * (1 - ratio));
    const b = Math.round(c1.b * ratio + c2.b * (1 - ratio));

    return rgbToHex(r, g, b);
}

function hexToRgb(hex) {
    const bigint = parseInt(hex.substring(1), 16);
    return {
        r: (bigint >> 16) & 255,
        g: (bigint >> 8) & 255,
        b: bigint & 255
    };
}

function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

function computeColors() {
    for (const color of ['night', 'purple', 'ocre', 'gold', 'yellow', 'weed', 'salad', 'forest'])
        colors[color] = getComputedStyle(document.documentElement).getPropertyValue(`--${color}`);
}

//----------------------------
//                       START
//----------------------------

if (typeof executeLocally === 'function') {
    executeLocally();
}
